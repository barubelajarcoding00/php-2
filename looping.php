<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Berlatih Looping</h1>
    <?php
    echo "<h3>Soal No 1 </h3>";
    echo "<h4>LOOPING PERTAMA</h4>";
    for ($i=2; $i <=20 ; $i+=2) { 
        echo $i . "- I Love PHP <br>";
    }
    echo "<h4>LOOPING KEDUA</h4>";
    for ($a=20; $a >= 2 ; $a-=2) { 
        echo $a . " - I Love PHP <br>";
    }

    echo "<h3>Soal No 2 </h3>";
    $nomor = [18, 45, 29, 61, 47, 34];
    echo "array : ";
    print_r($nomor);
    echo "<br>";
    foreach ($nomor as $nom) {
        $rest[] = $nom / 5;
    }
    print_r($rest);

    echo "<h3>Soal No 3 </h3>";
    $Items = [
        ["001", "Keyboard Logitek", 60000, "Keyboard yang mantap untuk kantoran", "logitek.jpeg"],
        ["002", "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik", "msi.jpeg"],
        ["003", "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
        ["004", "Mouse Jerry", 30000, "Mouse yang disukai kucing", "jerry.jpeg"]
    ];

    foreach ($Items as $key => $val) {
        $Items = array(
            'ID' => $val[0],
            'Name' => $val[1],
            'Price' => $val[2],
            'Description' => $val[3],
            'Source' => $val[0],
        );
        print_r($Items);
        echo "<br>";
    }

    echo "<h3>Soal No 4 </h3>";
    for ($c=1; $c <=5  ; $c++) {
        for ($b=$c; $b <=5; $b++) { 
            echo "*";
        } 
       echo "<br>";
    }

    ?>
</body>

</html>